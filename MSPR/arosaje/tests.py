from django import db
from django.test import TestCase

from .models import Utilisateur, TypeUtilisateur, Plante, Photo, Conseil

class UtilisateurTestCase(TestCase):
    def test_usertype_creation(self):
        TypeUtilisateur.objects.create(Type="Test 1")
        result = TypeUtilisateur.objects.filter(Type="Test 1")

        self.assertEqual(1, len(result))
        self.assertEqual("Test 1", result[0].Type)
    def test_user_creation_without_type(self):
        Utilisateur.objects.create(Pseudo="Test 2")
        result = Utilisateur.objects.filter(Pseudo="Test 2")

        self.assertEqual(1, len(result))
        self.assertEqual("Test 2", result[0].Pseudo)
    def test_user_creation_with_type(self):
        TypeUtilisateur.objects.create(Type="Test TypeUtilisateur 3")
        Utilisateur.objects.create(Pseudo="Test 3", Cle_TypeUtilisateur=TypeUtilisateur.objects.get(Type="Test TypeUtilisateur 3"))
        result = Utilisateur.objects.filter(Pseudo="Test 3")

        self.assertEqual(1, len(result))
        self.assertEqual("Test 3", result[0].Pseudo)
        self.assertEqual(TypeUtilisateur.objects.get(Type="Test TypeUtilisateur 3"), result[0].Cle_TypeUtilisateur)

    def test_plant_creation_without_dependencies(self):
        self.assertRaises(db.utils.IntegrityError, lambda: Plante.objects.create(Nom="Test 4", Type="Test Type 4"))

    def test_plant_creation_with_user(self):
        TypeUtilisateur.objects.create(Type="Test TypeUtilisateur 5")
        Utilisateur.objects.create(Pseudo="Test Utilisateur 5", Cle_TypeUtilisateur=TypeUtilisateur.objects.get(Type="Test TypeUtilisateur 5"))
        Plante.objects.create(Nom="Test 5", Type="Test Type 5", Cle_Utilisateur=Utilisateur.objects.get(Pseudo="Test Utilisateur 5"))
        result = Plante.objects.filter(Nom="Test 5")

        self.assertEqual(1, len(result))
        self.assertEqual("Test 5", result[0].Nom)
        self.assertEqual("Test Type 5", result[0].Type)
        self.assertEqual(Utilisateur.objects.get(Pseudo="Test Utilisateur 5"), result[0].Cle_Utilisateur)

    def test_photo_creation_without_dependencies(self):
        self.assertRaises(db.utils.IntegrityError, lambda: Photo.objects.create(Chemin_Acces="Test 6", Longitude="Test Longitude 6", Latitude="Test Latitude 6"))

    def test_photo_creation_with_user_plant(self):
        TypeUtilisateur.objects.create(Type="Test TypeUtilisateur 7")
        Utilisateur.objects.create(Pseudo="Test Utilisateur 7", Cle_TypeUtilisateur=TypeUtilisateur.objects.get(Type="Test TypeUtilisateur 7"))
        Plante.objects.create(Nom="Test Plante 7", Type="Test Type 7", Cle_Utilisateur=Utilisateur.objects.get(Pseudo="Test Utilisateur 7"))
        Photo.objects.create(Chemin_Acces="Test 7", Longitude="Test Longitude 7", Latitude="Test Latitude 7", Cle_Utilisateur=Utilisateur.objects.get(Pseudo="Test Utilisateur 7"), Cle_Plante=Plante.objects.get(Nom="Test Plante 7"))
        result = Photo.objects.filter(Chemin_Acces="Test 7")

        self.assertEqual(1, len(result))
        self.assertEqual("Test 7", result[0].Chemin_Acces)
        self.assertEqual("Test Longitude 7", result[0].Longitude)
        self.assertEqual("Test Latitude 7", result[0].Latitude)
        self.assertEqual(Utilisateur.objects.get(Pseudo="Test Utilisateur 7"), result[0].Cle_Utilisateur)
        self.assertEqual(Plante.objects.get(Nom="Test Plante 7"), result[0].Cle_Plante)

    def test_advice_creation_without_dependencies(self):
        self.assertRaises(db.utils.IntegrityError, lambda: Conseil.objects.create(Titre="Test 8", Conseil="Test Conseil 8"))

    def test_advice_creation_with_user_plant(self):
        TypeUtilisateur.objects.create(Type="Test TypeUtilisateur 9")
        Utilisateur.objects.create(Pseudo="Test Utilisateur 9", Cle_TypeUtilisateur=TypeUtilisateur.objects.get(Type="Test TypeUtilisateur 9"))
        Plante.objects.create(Nom="Test Plante 9", Type="Test Type 9", Cle_Utilisateur=Utilisateur.objects.get(Pseudo="Test Utilisateur 9"))
        Conseil.objects.create(Titre="Test 9", Conseil="Test Conseil 9", Cle_Utilisateur=Utilisateur.objects.get(Pseudo="Test Utilisateur 9"), Cle_Plante=Plante.objects.get(Nom="Test Plante 9"))
        result = Conseil.objects.filter(Titre="Test 9")

        self.assertEqual(1, len(result))
        self.assertEqual("Test 9", result[0].Titre)
        self.assertEqual("Test Conseil 9", result[0].Conseil)
        self.assertEqual(Utilisateur.objects.get(Pseudo="Test Utilisateur 9"), result[0].Cle_Utilisateur)
        self.assertEqual(Plante.objects.get(Nom="Test Plante 9"), result[0].Cle_Plante)
