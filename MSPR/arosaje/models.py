from django.db import models
from django.utils import timezone


class TypeUtilisateur(models.Model):
    Type = models.CharField(max_length=50)


class Utilisateur(models.Model):
    Pseudo = models.CharField(max_length=200)
    Cle_TypeUtilisateur = models.ForeignKey(TypeUtilisateur, on_delete=models.DO_NOTHING, blank=True, null=True)


class Plante(models.Model):
    Nom = models.CharField(max_length=100)
    Type = models.CharField(max_length=100)
    Cle_Utilisateur = models.ForeignKey(Utilisateur, on_delete=models.DO_NOTHING)


class Photo(models.Model):
    Chemin_Acces = models.CharField(max_length=200)
    Longitude = models.CharField(max_length=50)
    Latitude = models.CharField(max_length=50)
    Date_Creation = models.DateTimeField(default=timezone.now)
    Date_Observation = models.DateTimeField(blank=True, null=True, default=None)
    Cle_Utilisateur = models.ForeignKey(Utilisateur, on_delete=models.DO_NOTHING)
    Cle_Plante = models.ForeignKey(Plante, on_delete=models.CASCADE)


class Conseil(models.Model):
    Titre = models.CharField(max_length=100)
    Conseil = models.CharField(max_length=1000)
    Date = models.DateTimeField(default=timezone.now)
    Cle_Utilisateur = models.ForeignKey(Utilisateur, on_delete=models.DO_NOTHING)
    Cle_Plante = models.ForeignKey(Plante, on_delete=models.CASCADE)
