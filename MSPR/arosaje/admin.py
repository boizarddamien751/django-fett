from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(TypeUtilisateur)
admin.site.register(Utilisateur)
admin.site.register(Plante)
admin.site.register(Photo)
admin.site.register(Conseil)