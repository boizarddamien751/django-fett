from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('image', views.saveImage, name='saveImage'),
    path('get/images', views.getImages, name='getImages'),
]