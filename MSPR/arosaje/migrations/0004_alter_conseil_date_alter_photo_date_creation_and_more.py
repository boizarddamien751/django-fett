# Generated by Django 4.1.4 on 2022-12-22 10:47

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('arosaje', '0003_question_choice'),
    ]

    operations = [
        migrations.AlterField(
            model_name='conseil',
            name='Date',
            field=models.DateTimeField(default=datetime.date(2022, 12, 22)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='Date_Creation',
            field=models.DateTimeField(default=datetime.date(2022, 12, 22)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='Date_Observation',
            field=models.DateTimeField(blank=True, default=None, null=True),
        ),
        migrations.DeleteModel(
            name='Choice',
        ),
        migrations.DeleteModel(
            name='Question',
        ),
    ]
