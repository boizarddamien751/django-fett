from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

import base64
from PIL import Image
from .models import *

def index(request):
    pass

# def all_questions(request):
#     questions = Question.objects.all()
#     return HttpResponse(questions)
#
# def get_question(request, id):
#     questions = Question.objects.get(id=id)
#     return HttpResponse(questions)

def saveImage(request):
    user = Utilisateur.objects.filter(Pseudo=request.POST.get("user"))
    if len(user) == 1:
        user = Utilisateur.objects.get(Pseudo=request.POST.get("user"))
    elif len(user) > 1:
        raise "Too many users : two users with the same pseudo is aberrant."
    else:
        Utilisateur.objects.create(Pseudo=request.POST.get("user"), Cle_TypeUtilisateur=TypeUtilisateur.objects.get(id=1))
        user = Utilisateur.objects.get(Pseudo=request.POST.get("user"))

    result = open("arosaje/images/result.jpg", "wb")
    result.write(base64.b64decode(request.POST.get("image") + "=="))

    return HttpResponse("L'image a été enregistrée")

def getImages(request):
    image = Image.open("arosaje/images/result.jpg")
    imageComp = image.resize((int(image.size[0] / 10.0), int(image.size[1] / 10.0)))
    imageComp.save("arosaje/images/compressed/compressed.jpg")

    sendImage = open("arosaje/images/compressed/compressed.jpg", "rb")
    return HttpResponse(base64.b64encode(sendImage.read()))
